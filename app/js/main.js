// (function($,sr){

//   // debouncing function from John Hann
//   // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
//   var debounce = function (func, threshold, execAsap) {
//       var timeout;

//       return function debounced () {
//           var obj = this, args = arguments;
//           function delayed () {
//               if (!execAsap)
//                   func.apply(obj, args);
//               timeout = null;
//           };

//           if (timeout)
//               clearTimeout(timeout);
//           else if (execAsap)
//               func.apply(obj, args);

//           timeout = setTimeout(delayed, threshold || 100);
//       };
//   }
//   // smartresize
//   jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

// })(jQuery,'smartresize');

$(function () {
  createSlideHeader();
  // createSlideTopic();
  // createTabSlideTopic();
  // createSlideTeacher();
  // filterTeacher();
  // createSlideBenefit();

  $('main').scrollspy({ target: '#navbar-example' })

  // Init countdown
  // var deadline = new Date(Date.parse(new Date(2018, 8, 20)));
  // initializeClock('countdown', deadline);

  AOS.init({
    disable: 'mobile',
    duration: 1000,
    once: true
  });
});

function createSlideHeader() {
  var $slide = $('.slide-header');
  if ($slide.length > 0) {
    var settings = {
      variableWidth: true,
      slidesToShow: 1,
      prevArrow: '<button class="slick-prev"><span class="icon-arrow-left"></span></button>',
      nextArrow: '<button class="slick-next"><span class="icon-arrow-right"></span></button>',
      autoplay: false,
      autoplaySpeed: 4000,
      speed: 700,
      infinite: true,
      responsive: [
        {
          breakpoint: 960,
          settings: {
            variableWidth: false
          }
        }
      ]
    };
    $slide.slick(settings);
  }
}
